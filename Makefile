# Execution file name
EXE = post

# Directory name
INC_DIR = inc
SRC_DIR = src
OBJ_DIR = obj
LIB_DIR = lib
BIN_DIR = bin

# Compiler with MPI
CXX = mpicxx

# Compile option
ifeq ($(mode), debug)
  CFLAGS = -O0 -fp-model precise -traceback -debug full -check=conversion,stack,uninit -g -w2 -Wcheck -std=c++14 -fpic
else
  CFLAGS = -O3 -fp-model precise -traceback -std=c++14 -fpic
endif

# Library linking
LFLAGS = -lcgns -lhdf5

# Including path for compile
IFLAGS = -I $(INC_DIR)

# Definition flag
DFLAGS = -D $(def)

# What archiving to use
AR = ar rcs

# Object list
OBJS = $(OBJ_DIR)/DataType.o\
	     $(OBJ_DIR)/ElemType.o\
	     $(OBJ_DIR)/Data.o\
	     $(OBJ_DIR)/Surface.o
EXEC = $(OBJ_DIR)/Main.o

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CXX) $(CFLAGS) -c -o $@ $< $(IFLAGS) $(DFLAGS)

.PHONY: all clean distclean

all: $(OBJS) $(EXEC) $(LIBS)
	$(CXX) -o $(BIN_DIR)/$(EXE) $(OBJS) $(EXEC) $(LFLAGS)
	$(CXX) -o $(LIB_DIR)/lib$(EXE).so $(OBJS) $(LIBS) -shared

clean:
	find $(OBJ_DIR) -name '*~' -exec rm {} \;
	find $(OBJ_DIR) -name '*.o' -exec rm {} \;
	find $(OBJ_DIR) -name '*.optrpt' -exec rm {} \;
	find $(BIN_DIR) -name '*.optrop' -exec rm {} \;

distclean: clean
	rm -f $(BIN_DIR)/$(EXE)
	rm -f $(LIB_DIR)/lib$(EXE).so

