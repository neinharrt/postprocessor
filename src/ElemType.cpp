#include "ElemType.hpp"

std::string ElemType::getName(int_t elem_type)
{
  switch (elem_type)
  {
    case LINE: return "LINE"; break;
    case TRIS: return "TRIS"; break;
    case QUAD: return "QUAD"; break;
    case TETS: return "TETS"; break;
    case PYRA: return "PYRA"; break;
    case PRIS: return "PRIS"; break;
    case HEXA: return "HEXA"; break;
    default: return "";
  }
}

int_t ElemType::getIndexSize(int_t elem_type)
{
  switch (elem_type)
  {
    case LINE: return 2; break;
    case TRIS: return 3; break;
    case QUAD: return 4; break;
    case TETS: return 4; break;
    case PYRA: return 5; break;
    case PRIS: return 6; break;
    case HEXA: return 8; break;
    default: return -1;
  }
}

int_t ElemType::getNumSubElems(int_t elem_type)
{
  return 0;
}

int_t ElemType::getSubElemType(int_t elem_type, int_t isub)
{
  return 0;
}

const std::vector<int_t>& ElemType::getSubElemIndex(int_t elem_type, int_t isub)
{
  static std::vector<int_t> a(10,0);
  return a;
}
