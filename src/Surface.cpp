#include "Surface.hpp"
#include "ElemType.hpp"

Surface::Surface()
{
}

Surface::~Surface()
{
}

void Surface::allocatePostData()
{
  // Allocate & initialize face data & connectivity.
  int_t index_size_max0 = 0;
  int_t index_size_max1 = 0;
  for (int_t itype = 0; itype < surface_face_type_list_.size(); itype++)
  {
    const int_t &elem_type = surface_face_type_list_[itype];

    cgsize_t face_range_min = surface_face_global_ptr_[itype]+1;
    cgsize_t face_range_max = surface_face_global_ptr_[itype+1];
    if (face_range_min > face_range_max) continue;

    cgsize_t face_start = surface_face_local_start_[itype]+1;
    cgsize_t face_end = surface_face_local_end_[itype];
    int_t index_size = ElemType::getIndexSize(elem_type);

    index_size_max0 = std::max(index_size_max0, face_end - face_start + 1);
    index_size_max1 = std::max(index_size_max1, index_size*(face_end - face_start + 1));
  }
  sorted_data_.resize(index_size_max0, 0.0);
  face_elements_.resize(index_size_max1, 0.0);

  coordinate_x_.resize(num_nodes_, 0.0);
  coordinate_y_.resize(num_nodes_, 0.0);
  coordinate_z_.resize(num_nodes_, 0.0);
}

void Surface::open(const std::string &filename)
{
  MASTER_MESSAGE("Averaging surface files .......\n");
  // Open CGNS file.
  int cell_dim = cell_dim_;
  int phys_dim = phys_dim_;
  if (cgp_open(filename.c_str(), CG_MODE_WRITE, &fn_) || cg_base_write(fn_, "Base", cell_dim, phys_dim, &B_)) cgp_error_exit();
}

void Surface::writeZone()
{
  // Set surface global pointer front/back index.
  int_t front_index = surface_face_global_ptr_.front();
  int_t back_index = surface_face_global_ptr_.back();

  // Write zone.
  std::array<cgsize_t, 3> size;
  size[0] = num_total_nodes_;
  size[1] = back_index - front_index;
  size[2] = 0;
  if (cg_zone_write(fn_, B_, "Zone", &size[0], Unstructured, &Z_)) cgp_error_exit();
}

void Surface::writeCoordinates()
{
  if (dimension_ == 2)
  {
    // Create coordinates data in parallel.
    if (cgp_coord_write(fn_, B_, Z_, RealDouble, "CoordinateX", &Cx_) ||
        cgp_coord_write(fn_, B_, Z_, RealDouble, "CoordinateY", &Cy_)) cgp_error_exit();

    // Set node start/end index.
    cgsize_t node_start = surface_node_local_start_ + 1;
    cgsize_t node_end = surface_node_local_end_;

    // Set node coordinates for CGNS.
    for (int_t inode = 0; inode < num_nodes_; inode++)
    {
      coordinate_x_[inode] = node_centroid_[0][inode];
      coordinate_y_[inode] = node_centroid_[1][inode];
    }

    // Write coordinates data in parallel.
    if (node_start > node_end)
    {
      if (cgp_coord_write_data(fn_, B_, Z_, Cx_, nullptr, nullptr, nullptr) ||
          cgp_coord_write_data(fn_, B_, Z_, Cy_, nullptr, nullptr, nullptr)) cgp_error_exit();
    }
    else
    {
      if (cgp_coord_write_data(fn_, B_, Z_, Cx_, &node_start, &node_end, &coordinate_x_[0]) ||
          cgp_coord_write_data(fn_, B_, Z_, Cy_, &node_start, &node_end, &coordinate_y_[0])) cgp_error_exit();
    }
  }
  else if (dimension_ == 3)
  {
    // Create coordinates data in parallel.
    if (cgp_coord_write(fn_, B_, Z_, RealDouble, "CoordinateX", &Cx_) ||
        cgp_coord_write(fn_, B_, Z_, RealDouble, "CoordinateY", &Cy_) ||
        cgp_coord_write(fn_, B_, Z_, RealDouble, "CoordinateZ", &Cz_)) cgp_error_exit();

    // Set node start/end index.
    cgsize_t node_start = surface_node_local_start_ + 1;
    cgsize_t node_end = surface_node_local_end_;

    // Set node coordinates for CGNS.
    for (int_t inode = 0; inode < num_nodes_; inode++)
    {
      coordinate_x_[inode] = node_centroid_[0][inode];
      coordinate_y_[inode] = node_centroid_[1][inode];
      coordinate_z_[inode] = node_centroid_[2][inode];
    }

    // Write coordinates data in paralle.
    if (node_start > node_end)
    {
      if (cgp_coord_write_data(fn_, B_, Z_, Cx_, nullptr, nullptr, nullptr) ||
          cgp_coord_write_data(fn_, B_, Z_, Cy_, nullptr, nullptr, nullptr) ||
          cgp_coord_write_data(fn_, B_, Z_, Cz_, nullptr, nullptr, nullptr)) cgp_error_exit();
    }
    else
    {
      if (cgp_coord_write_data(fn_, B_, Z_, Cx_, &node_start, &node_end, &coordinate_x_[0]) ||
          cgp_coord_write_data(fn_, B_, Z_, Cy_, &node_start, &node_end, &coordinate_y_[0]) ||
          cgp_coord_write_data(fn_, B_, Z_, Cz_, &node_start, &node_end, &coordinate_z_[0])) cgp_error_exit();
    }
  }
}

void Surface::writeFaceElements()
{
  // Loop over face type list.
  for (int_t itype = 0; itype < surface_face_type_list_.size(); itype++)
  {
    // Set element type.
    const int_t &elem_type = surface_face_type_list_[itype];
    
    // Set and check face range min/max.
    cgsize_t face_range_min = surface_face_global_ptr_[itype]+1;
    cgsize_t face_range_max = surface_face_global_ptr_[itype+1];
    if (face_range_min > face_range_max) continue;

    // Create section in parallel.
    std::string name = ElemType::getName(elem_type);
    ElementType_t type = static_cast<ElementType_t>(elem_type);
    if (cgp_section_write(fn_, B_, Z_, name.c_str(), type, face_range_min, face_range_max, 0, &S_)) cgp_error_exit();

    // Set face start/end index.
    cgsize_t face_start = surface_face_local_start_[itype]+1;
    cgsize_t face_end = surface_face_local_end_[itype];

    // Set element connectivity for CGNS.
    int_t index = -1;
    for (auto &&iface : surface_face_list_[itype])
    {
      for (int_t f2n_index = face_to_node_ptr_[iface];
                 f2n_index < face_to_node_ptr_[iface+1];
                 f2n_index++)
      {
        ++index;
        const int_t &inode = face_to_node_ind_[f2n_index];
        face_elements_[index] = inode + 1;
      }
    }

    if (face_start > face_end)
    {
      if (cgp_elements_write_data(fn_, B_, Z_, S_, face_range_min, face_range_max, nullptr)) cgp_error_exit();
    }
    else
    {
      if (cgp_elements_write_data(fn_, B_, Z_, S_, face_start, face_end, &face_elements_[0])) cgp_error_exit();
    }
  }
}

void Surface::writeSolutionHeader()
{
  if (cg_sol_write(fn_, B_, Z_, "Solution", CellCenter, &S_)) cgp_error_exit();
}

void Surface::close()
{
  // Close CGNS file.
  if (cgp_close(fn_)) cgp_error_exit();
}

void Surface::writeEqtnData(const std::vector<std::string> &name, const std::vector<std::vector<real_t>> &data)
{
  for (int_t icol = 0; icol < data.size(); icol++)
  {
    // Create field data in parallel.
    if (cgp_field_write(fn_, B_, Z_, S_, RealDouble, name[icol].c_str(), &F_)) cgp_error_exit();

    // Loop over face type list.
    for (int_t itype = 0; itype < surface_face_type_list_.size(); itype++)
    {
      cgsize_t face_range_min = surface_face_global_ptr_[itype]+1;
      cgsize_t face_range_max = surface_face_global_ptr_[itype+1];
      if (face_range_min > face_range_max) continue;

      // Set face start/end index.
      cgsize_t face_start = surface_face_local_start_[itype]+1;
      cgsize_t face_end = surface_face_local_end_[itype];

      // Set face data for CGNS.
      int_t index = -1;
      for (auto &&iface : surface_face_list_[itype])
      {
        ++index;
        sorted_data_[index] = data[icol][iface];
      }

      // Write field data in parallel.
      if (face_start > face_end)
      {
        if (cgp_field_write_data(fn_, B_, Z_, S_, F_, nullptr, nullptr, nullptr)) cgp_error_exit();
      }
      else
      {
        if (cgp_field_write_data(fn_, B_, Z_, S_, F_, &face_start, &face_end, &sorted_data_[0])) cgp_error_exit();
      }
    }
  }
}

void Surface::readInputFile(std::string &name)
{
  std::ifstream infile;
  std::string text;

  const std::string filename = "Post.cfg";
  infile.open(filename);
  if (!infile.is_open())
    MASTER_ERROR("Input file does not exist: " + filename);

  text = getValueFromKey(infile, "Start file number");
  start_file_number_ = std::stol(text);
  text = getValueFromKey(infile, "End file number");
  end_file_number_ = std::stol(text);

  std::stringstream extension;
  extension << "_" << std::setw(8) << std::setfill('0') << start_file_number_;
  name = "surface" + extension.str() + ".cgns";
}

void Surface::readGridFile(const std::string &filename)
{
  // Set rank of the processor and size of all processors.
  const int_t my_rank = MPI::COMM_WORLD.Get_rank();
  const int_t ndomain = MPI::COMM_WORLD.Get_size();

  // Declare CGNS basic index.
  int fn, B, Z, S, C, BC;

  // Delcare CGNS header data.
  int cell_dim, phys_dim;
  int nbases, nzones, ngrids;
  char basename[33], zonename[33];
  cgsize_t size[3];
  ZoneType_t zonetype;

  // Declare CGNS coordinate data.
  int ncoords;
  char coordname[33];
  DataType_t datatype;
  std::vector<std::vector<double>> coord_array;
  cgsize_t node_range_min;
  cgsize_t node_range_max;

  // Declare CGNS section data.
  int nsections;
  char ElementSectionName[33];
  cgsize_t ElementDataSize;
  std::vector<ElementType_t> type;
  std::vector<cgsize_t> elem_range_min;
  std::vector<cgsize_t> elem_range_max;
  std::vector<cgsize_t> start;
  std::vector<cgsize_t> end;
  std::vector<std::vector<cgsize_t>> Elements;
  int nbndry, parent_flag, npe;
  cgsize_t start_tmp, end_tmp;
  std::vector<cgsize_t> Elements_tmp;

  // Declare CGNS boundary condition data.
  int nbocos;
  char boconame_tmp[33];
  std::vector<std::string> boconame;
  BCType_t bocotype;
  std::vector<PointSetType_t> ptset_type;
  std::vector<cgsize_t> npnts;
  std::vector<std::vector<cgsize_t>> pnts;
  int NormalIndex;
  cgsize_t NormalListSize;
  DataType_t NormalDataType;
  int ndataset;

  // Declare distribution data.
  int_t num_total_nodes;
  int_t num_total_elems;
  std::vector<int_t> node_dist;
  std::vector<int_t> elem_dist;

  // Declare grid setting index data.
  int_t xyz_index;
  int_t index_tmp;
  int_t index_size;
  int_t num_local;
  int_t icell, num_cells;
  int_t iface, num_faces;
  int_t c2n_index, c2n_index_tmp;
  int_t f2n_index, f2n_index_tmp;
  int_t c2n_total_index;
  int_t f2n_total_index;
  bool boco_flag;
  int_t inode1, inode2;
  int_t inode4, inode5;

  // Set CGNS MPI communicator.
  cgp_mpi_comm(MPI::COMM_WORLD);

  // Open CGNS file.
  std::string cgns_name = "./surface/"+filename;
  if (cgp_open(cgns_name.c_str(), CG_MODE_READ, &fn)) cgp_error_exit();

  // Read the number of bases.
  if (cg_nbases(fn, &nbases)) cg_error_exit();
  B = 1;

  // Read base naem and dimension.
  if (cg_base_read(fn, B, basename, &cell_dim, &phys_dim)) cg_error_exit();

  // Read the number of zones.
  if (cg_nzones(fn, B, &nzones)) cg_error_exit();
  Z = 1;
  
  // Read zone information.
  if (cg_zone_read(fn, B, Z, zonename, &size[0])) cg_error_exit();
  if (cg_zone_type(fn, B, Z, &zonetype)) cg_error_exit();

  // Read the number of grids.
  if (cg_ngrids(fn, B, Z, &ngrids)) cg_error_exit();

  num_total_nodes = size[0];
  node_dist = getDistribution(num_total_nodes, ndomain);

  // Read the number of coordinates.
  if (cg_ncoords(fn, B, Z, &ncoords)) cg_error_exit();

  // Allocate coordinates array.
  coord_array.resize(ncoords);

  // Loop over coordinates.
  for (int_t icoord = 0; icoord < ncoords; icoord++)
  {
    // Set coordinate index.
    C = icoord + 1;

    // Read coordinate info.
    if (cg_coord_info(fn, B, Z, C, &datatype, coordname)) cg_error_exit();

    // Set and check node min/max range.
    node_range_min = node_dist[my_rank]+1;
    node_range_max = node_dist[my_rank+1];
    if (node_range_min > node_range_max)
    {
      if (cgp_coord_read_data(fn, B, Z, C, nullptr, nullptr, nullptr)) cgp_error_exit();
    }
    else
    {
      coord_array[icoord].resize(node_range_max - node_range_min + 1);
      if (cgp_coord_read_data(fn, B, Z, C, &node_range_min, &node_range_max, &coord_array[icoord][0])) cgp_error_exit();
    }
  }

  // Read the number of sections.
  if (cg_nsections(fn, B, Z, &nsections)) cg_error_exit();

  // Allocate element data.
  type.resize(nsections);
  elem_range_min.resize(nsections);
  elem_range_max.resize(nsections);
  start.resize(nsections);
  end.resize(nsections);
  Elements.resize(nsections);

  // Loop over sections.
  for (int_t isection = 0; isection < nsections; isection++)
  {
    S = isection + 1;

    // Read section info and element type.
    if (cg_section_read(fn, B, Z, S, ElementSectionName, &type[isection], &elem_range_min[isection], &elem_range_max[isection], &nbndry, &parent_flag)) cg_error_exit();

    // Compute distribution of elements.
    num_total_elems = elem_range_max[isection] - elem_range_min[isection] + 1;
    elem_dist = getDistribution(num_total_elems, ndomain);

    // Set and check start/end index.
    start[isection] = elem_range_min[isection] + elem_dist[my_rank];
    end[isection] = elem_range_min[isection] + elem_dist[my_rank+1] - 1;

    if (type[isection] == MIXED)
    {
      if (start[isection] > end[isection])
      {
        start_tmp = end_tmp = elem_range_max[isection];
        if (cg_ElementPartialSize(fn, B, Z, S, start_tmp, end_tmp, &ElementDataSize)) cg_error_exit();
        Elements_tmp.resize(ElementDataSize, -1);
        if (cg_elements_partial_read(fn, B, Z, S, start_tmp, end_tmp, &Elements_tmp[0], nullptr)) cg_error_exit();
      }
      else
      {
        if (cg_ElementPartialSize(fn, B, Z, S, start[isection], end[isection], &ElementDataSize)) cg_error_exit();
        Elements[isection].resize(ElementDataSize, -1);
        if (cg_elements_partial_read(fn, B, Z, S, start[isection], end[isection], &Elements[isection][0], nullptr)) cg_error_exit();
      }
    }
    else
    {
      if (start[isection] > end[isection])
      {
        if (cgp_elements_read_data(fn, B, Z, S, elem_range_min[isection], elem_range_max[isection], nullptr)) cgp_error_exit();
      }
      else
      {
        if (cg_npe(type[isection], &npe)) cg_error_exit();
        Elements[isection].resize(npe*(end[isection] - start[isection] + 1), -1);
        if (cgp_elements_read_data(fn, B, Z, S, start[isection], end[isection], &Elements[isection][0])) cgp_error_exit();
      }
    }
  }

  // Read the number of field.
  int num_fields;
  num_faces = size[1];
  S = 1;
  if (cg_nfields(fn, B, Z, S, &num_fields)) cgp_error_exit();
  solution_.resize(num_fields);
  for (int_t idata = 0; idata < num_fields; idata++)
  {
    solution_[idata].resize(num_faces, 0.0);
  }

  // Initialize the number of faces and f2n total index.
  f2n_total_index = 0;

  for (int isection = 0; isection < nsections; isection++)
  {
    if (start[isection] > end[isection]) continue;
    f2n_total_index += Elements[isection].size();
  }

  // Grid data setting.
  dimension_ = phys_dim;
  cell_dim_ = cell_dim;
  phys_dim_ = phys_dim;

  num_total_nodes_ = int_t(size[0]);
  num_total_faces_ = int_t(size[1]);
  num_nodes_ = node_range_max - node_range_min + 1;
  num_faces_ = int_t(ceil(real_t(num_total_faces_)/real_t(ndomain)));
  num_faces_ = std::min(num_faces_, num_total_faces_);
  face_global_index_.resize(num_faces_);
  std::vector<int_t> face_dist(ndomain+1, 0);
  for (int_t idomain = 0; idomain < ndomain; idomain++)
  {
    face_dist[idomain+1] = face_dist[idomain] + num_faces_;
    face_dist[idomain+1] = std::min(face_dist[idomain+1], num_total_faces_);
  }

  for (iface = 0; iface < num_faces_; iface++)
  {
    face_global_index_[iface] = face_dist[my_rank] + iface;
  }
  node_centroid_.resize(dimension_);
  for (int_t idim = 0; idim < dimension_; idim++)
  {
    node_centroid_[idim].resize(num_nodes_);
  }

  for (int_t inode = 0; inode < num_nodes_; inode++)
  {
    for (int_t idim = 0; idim < dimension_; idim++)
    {
      node_centroid_[idim][inode] = coord_array[idim][inode];
    }
  }
  surface_node_local_start_ = node_dist[my_rank];
  surface_node_local_end_ = node_dist[my_rank+1];

  if (dimension_ == 2)
  {
    surface_face_type_list_.reserve(1);
    surface_face_type_list_.push_back(ElemType::LINE);
  }
  else if (dimension_ == 3)
  {
    surface_face_type_list_.reserve(2);
    surface_face_type_list_.push_back(ElemType::TRIS);
    surface_face_type_list_.push_back(ElemType::QUAD);
  }

  iface = 0;
  face_elem_type_.resize(num_total_faces_);
  face_to_node_ptr_.resize(num_faces_+1);
  face_to_node_ind_.resize(f2n_total_index);
  for (int_t isection = 0; isection < nsections; isection++)
  {
    index_tmp = 0;
    for (cgsize_t iglobal = start[isection]; iglobal <= end[isection]; iglobal++)
    {
      face_elem_type_[iface] = type[isection];
      index_size = ElemType::getIndexSize(face_elem_type_[iface]);

      f2n_total_index = face_to_node_ptr_[iface] + index_size;
      face_to_node_ptr_[iface+1] = f2n_total_index;

      for (int_t index = 0; index < index_size; index++)
      {
        f2n_index = face_to_node_ptr_[iface] + index;
        face_to_node_ind_[f2n_index] = Elements[isection][index_tmp] - 1;
        ++index_tmp;
      }
      ++iface;
    }
  }

  const int_t &num_face_type = surface_face_type_list_.size();
  surface_face_global_ptr_.resize(num_face_type+1, 0);
  surface_face_local_start_.resize(num_face_type, 0);
  surface_face_local_end_.resize(num_face_type, 0);
  surface_face_list_.resize(num_face_type);

  for (int_t itype = 0; itype < num_face_type; itype++)
  {
    for (int_t iface = 0; iface < num_total_faces_; iface++)
    {
      if (surface_face_type_list_[itype] == face_elem_type_[iface])
      {
        surface_face_list_[itype].push_back(iface);
      }
    }

    surface_face_global_ptr_[itype+1] = surface_face_list_[itype].size();
    surface_face_local_start_[itype] = surface_face_global_ptr_[itype];
    surface_face_local_end_[itype] = surface_face_local_start_[itype] + surface_face_list_[itype].size();

    for (int_t idomain = 0; idomain < ndomain; idomain++)
    {
      int_t num_face = surface_face_list_[itype].size();
      MPI::COMM_WORLD.Bcast(&num_face, 1, MPIDataType<int_t>(), idomain);
      if (idomain < my_rank)
      {
        surface_face_local_start_[itype] += num_face;
        surface_face_local_end_[itype] += num_face;
      }
    }

    int_t num_face_total;
    int_t num_face = surface_face_list_[itype].size();
    MPI::COMM_WORLD.Allreduce(&num_face, &num_face_total, 1, MPIDataType<int_t>(), MPI_SUM);
    surface_face_global_ptr_[itype+1] = surface_face_global_ptr_[itype] + num_face_total;
  }
}

void Surface::readSolutionData(const std::string &filename)
{
  std::string CGNSfilename = filename + ".cgns";
  CGNSfilename = "./surface/" + CGNSfilename;

  // Set rank of the processor and size of all processors.
  int_t my_rank = MPI::COMM_WORLD.Get_rank();
  int_t ndomain = MPI::COMM_WORLD.Get_size();

  cgp_mpi_comm(MPI::COMM_WORLD);

  int fn = 1;
  int B = 1;
  int Z = 1;
  int S = 1;
  int F;

  // Open CGNS file
  if (cgp_open(CGNSfilename.c_str(), CG_MODE_READ, &fn)) cgp_error_exit();

  // Read zone.
  char zonename[33];
  std::array<cgsize_t, 3> size;
  if (cg_zone_read(fn, B, Z, zonename, &size[0])) cgp_error_exit();
  int_t num_total_faces = int_t(size[1]);

  // Set the number of faces with face dist data.
  int_t num_faces;
  int_t num_faces_max = 0;
  num_faces = int_t(ceil(real_t(num_total_faces)/real_t(ndomain)));
  std::vector<int_t> face_dist(ndomain+1, 0);
  for (int_t idomain = 0; idomain < ndomain; idomain++)
  {
    face_dist[idomain+1] = face_dist[idomain] + num_faces;
    face_dist[idomain+1] = std::min(face_dist[idomain+1], num_total_faces);
    num_faces_max = std::max(num_faces_max, face_dist[idomain+1] - face_dist[idomain]);
  }
  num_faces = face_dist[my_rank+1] - face_dist[my_rank];
  num_faces_ = num_faces;

  // Read the number of field.
  int num_fields;
  if (cg_nfields(fn, B, Z, S, &num_fields)) cgp_error_exit();
  num_solutions_ = num_fields;

  std::vector<real_t> face_data(num_faces, 0.0);

  solution_names_.resize(num_solutions_);
  std::vector<std::vector<real_t>> solution_tmp;
  solution_tmp.resize(num_solutions_);
  for (int_t isoln = 0; isoln < num_solutions_; isoln++)
  {
    solution_tmp[isoln].resize(num_faces_, 0.0);
  }

  // Loop over field
  for (int_t ifield = 0; ifield < num_fields; ifield++)
  {
    F = ifield + 1;

    // Read field name & data type.
    DataType_t data_type;
    char fieldname[33];
    if (cg_field_info(fn, B, Z, S, F, &data_type, fieldname)) cgp_error_exit();
    solution_names_[ifield] = fieldname;

    // Set and check face range min/max.
    cgsize_t face_range_min = face_dist[my_rank]+1;
    cgsize_t face_range_max = face_dist[my_rank+1];

    // Read field data.
    if (cgp_field_read_data(fn, B, Z, S, F, &face_range_min, &face_range_max, &face_data[0])) cgp_error_exit();

    // Loop over domains.
    for (int_t iface = 0; iface < num_faces_; iface++)
    {
      int_t index = face_global_index_[iface] - face_dist[my_rank];
      solution_tmp[ifield][iface] = face_data[index];
    }
    for (int_t iface = 0; iface < num_faces_; iface++)
    {
      solution_[ifield][iface] += solution_tmp[ifield][iface];
    }
  }
}

void Surface::writeIterData()
{
  if (cg_simulation_type_write(fn_, B_, NonTimeAccurate)) cgp_error_exit();

  // Access BaseIterativeData node.
  cgsize_t dimension_vector = 1;
  if (cg_biter_write(fn_, B_, "TimeIterValue", 1)) cgp_error_exit();
  if (cg_goto(fn_, B_, "BaseIterativeData_t", 1, "end")) cgp_error_exit();

  // Write time value.
  real_t time = 0.0;
  if (cg_array_write("TimeValues", RealDouble, 1, &dimension_vector, &time)) cgp_error_exit();

  int_t physical_iter = 0;
  if (cg_array_write("IterativeValues", Integer, 1, &dimension_vector, &physical_iter)) cgp_error_exit();

  // Write pseudo iteration value.
  int_t pseudo_iter = 0;
  if (cg_array_write("PseudoIterationValue", Integer, 1, &dimension_vector, &pseudo_iter)) cgp_error_exit();
}

std::vector<int_t> Surface::getDistribution(const int_t &num_data, const int_t &num_divide)
{
  // Compute interval.
  int_t interval = int_t(ceil(real_t(num_data)/real_t(num_divide)));

  // Declare and set dist data.
  std::vector<int_t> dist(num_divide+1, 0);
  for (int_t idist = 0; idist < num_divide; idist++)
  {
    dist[idist+1] = dist[idist] + interval;
    dist[idist+1] = std::min(dist[idist+1], num_data);
  }

  return dist;
}
