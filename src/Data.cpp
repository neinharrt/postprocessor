#include "Data.hpp"
#include "ElemType.hpp"

Data::Data()
{
}

Data::~Data()
{
}

void Data::readInputFile(std::string &name)
{
  std::ifstream infile;
  std::string text;

  const std::string filename = "Post.cfg";
  infile.open(filename);
  if (!infile.is_open())
    MASTER_ERROR("Input file does not exist: " + filename);

  text = getValueFromKey(infile, "Start file number");
  start_file_number_ = std::stol(text);
  text = getValueFromKey(infile, "End file number");
  end_file_number_ = std::stol(text);

  std::stringstream extension;
  extension << "_" << std::setw(8) << std::setfill('0') << start_file_number_;
  name = "volume" + extension.str() + ".cgns";
}

void Data::readGridFile(const std::string& filename)
{
  ////////////////////////////////////////////////////////////
  //                  LOCAL DATA DECLARATION                //
  ////////////////////////////////////////////////////////////

  // Set rank of the processor and size of all processors.
  const int_t my_rank = MPI::COMM_WORLD.Get_rank();
  const int_t ndomain = MPI::COMM_WORLD.Get_size();

  // Declare CGNS basic index.
  int fn, B, Z, S, C, BC;

  // Declare CGNS header data.
  int cell_dim, phys_dim;
  int nbases, nzones, ngrids;
  char basename[33], zonename[33];
  cgsize_t size[3];
  ZoneType_t zonetype;

  // Declare CGNS coordinate data.
  int ncoords;
  char coordname[33];
  DataType_t datatype;
  std::vector<std::vector<double>> coord_array;
  cgsize_t node_range_min;
  cgsize_t node_range_max;

  // Declare CGNS section data.
  int nsections;
  char ElementSectionName[33];
  cgsize_t ElementDataSize;
  std::vector<ElementType_t> type;
  std::vector<cgsize_t> elem_range_min;
  std::vector<cgsize_t> elem_range_max;
  std::vector<cgsize_t> start;
  std::vector<cgsize_t> end;
  std::vector<std::vector<cgsize_t>> Elements;
  int nbndry, parent_flag, npe;
  cgsize_t start_tmp, end_tmp;
  std::vector<cgsize_t> Elements_tmp;

  // Declare CGNS boundary condition data.
  int nbocos;
  char boconame_tmp[33];
  std::vector<std::string> boconame;
  BCType_t bocotype;
  std::vector<PointSetType_t> ptset_type;
  std::vector<cgsize_t> npnts;
  std::vector<std::vector<cgsize_t>> pnts;
  int NormalIndex;
  cgsize_t NormalListSize;
  DataType_t NormalDataType;
  int ndataset;

  // Declare distribution data.
  int_t num_total_nodes;
  int_t num_total_elems;
  std::vector<int_t> node_dist;
  std::vector<int_t> elem_dist;

  // Declare grid setting index data.
  int_t xyz_index;
  int_t index_tmp;
  int_t index_size;
  int_t num_local;
  int_t icell, num_cells;
  int_t iface, num_faces;
  int_t c2n_index, c2n_index_tmp;
  int_t f2n_index, f2n_index_tmp;
  int_t c2n_total_index;
  int_t f2n_total_index;
  bool boco_flag;
  int_t inode1, inode2;
  int_t inode4, inode5;

  ////////////////////////////////////////////////////////////
  //                  READ CGNS GRID FILE                   //
  ////////////////////////////////////////////////////////////

  // Set CGNS MPI communicator.
  cgp_mpi_comm(MPI::COMM_WORLD);

  // Open CGNS file.
  std::string cgns_name = "./volume/"+filename;
  if (cgp_open(cgns_name.c_str(), CG_MODE_READ, &fn)) cgp_error_exit();

  // Read the number of bases.
  if (cg_nbases(fn, &nbases)) cg_error_exit();
  if (nbases > 1) MASTER_ERROR("CGNS with multiple bases is not supported");
  B = 1; // set base index

  // Read base name and dimension.
  if (cg_base_read(fn, B, basename, &cell_dim, &phys_dim)) cg_error_exit();
  if (cell_dim != phys_dim) MASTER_ERROR("Physical dimension must be equal to cell dimension");

  // Read the number of zones.
  if (cg_nzones(fn, B, &nzones)) cg_error_exit();
  if (nzones > 1) MASTER_ERROR("CGNS with multiple zones is not supported");
  Z = 1; // set zone index

  // Read zone information.
  if (cg_zone_read(fn, B, Z, zonename, &size[0])) cg_error_exit();
  if (cg_zone_type(fn, B, Z, &zonetype)) cg_error_exit();
  if (zonetype != Unstructured) MASTER_ERROR("Zone type must be \"Unstructured\"");

  // Read the number of grids.
  if (cg_ngrids(fn, B, Z, &ngrids)) cg_error_exit();
  if (ngrids > 1) MASTER_ERROR("CGNS with Multiple grids is not supported");

  // Compute distribution of nodes.
  num_total_nodes = size[0];
  node_dist = getDistribution(num_total_nodes, ndomain);

  // Read the number of coordinates.
  if (cg_ncoords(fn, B, Z, &ncoords)) cg_error_exit();

  // Allocate coordinates array.
  coord_array.resize(ncoords);

  // Loop over coordinates.
  for (int icoord = 0; icoord < ncoords; icoord++)
  {
    // Set coordinate index.
    C = icoord + 1;

    // Read coordinate info.
    if (cg_coord_info(fn, B, Z, C, &datatype, coordname)) cg_error_exit();
    if (datatype != RealDouble) MASTER_ERROR("Coordinate data type must be RealDouble");

    // Set and check node min/max range.
    node_range_min = node_dist[my_rank]+1;
    node_range_max = node_dist[my_rank+1];
    if (node_range_min > node_range_max)
    {
      // Dummy routine for MPI.
      if (cgp_coord_read_data(fn, B, Z, C, nullptr, nullptr, nullptr)) cgp_error_exit();
    }
    else
    {
      // Allocate and read coordinates.
      coord_array[icoord].resize(node_range_max - node_range_min + 1);
      if (cgp_coord_read_data(fn, B, Z, C, &node_range_min, &node_range_max, &coord_array[icoord][0])) cgp_error_exit();
    }
  }

  // Read the number of sections.
  if (cg_nsections(fn, B, Z, &nsections)) cg_error_exit();

  // Allocate element data.
  type.resize(nsections);
  elem_range_min.resize(nsections);
  elem_range_max.resize(nsections);
  start.resize(nsections);
  end.resize(nsections);
  Elements.resize(nsections);

  // Loop over sections.
  for (int isection = 0; isection < nsections; isection++)
  {
    // Set section index number.
    S = isection + 1;

    // Read section info and element type.
    if (cg_section_read(fn, B, Z, S, ElementSectionName, &type[isection], &elem_range_min[isection], &elem_range_max[isection], &nbndry, &parent_flag)) cg_error_exit();

    // Compute distribution of elements.
    num_total_elems = elem_range_max[isection] - elem_range_min[isection] + 1;
    elem_dist = getDistribution(num_total_elems, ndomain);

    // Set and check start/end index.
    start[isection] = elem_range_min[isection] + elem_dist[my_rank];
    end[isection] = elem_range_min[isection] + elem_dist[my_rank+1] - 1;

    // Check mixed type elements.
    if (type[isection] == MIXED)
    {
      if (start[isection] > end[isection])
      {
        // Dummy routine for reading.
        start_tmp = end_tmp = elem_range_max[isection];
        if (cg_ElementPartialSize(fn, B, Z, S, start_tmp, end_tmp, &ElementDataSize)) cg_error_exit();
        Elements_tmp.resize(ElementDataSize, -1);
        if (cg_elements_partial_read(fn, B, Z, S, start_tmp, end_tmp, &Elements_tmp[0], nullptr)) cg_error_exit();
      }
      else
      {
        // Allocate and read element data.
        if (cg_ElementPartialSize(fn, B, Z, S, start[isection], end[isection], &ElementDataSize)) cg_error_exit();
        Elements[isection].resize(ElementDataSize, -1);
        if (cg_elements_partial_read(fn, B, Z, S, start[isection], end[isection], &Elements[isection][0], nullptr)) cg_error_exit();
      }
    }
    else
    {
      if (start[isection] > end[isection])
      {
        // Dummy routine for MPI.
        if (cgp_elements_read_data(fn, B, Z, S, elem_range_min[isection], elem_range_max[isection], nullptr)) cgp_error_exit();
      }
      else
      {
        // Allocate and read element data.
        if (cg_npe(type[isection], &npe)) cg_error_exit();
        Elements[isection].resize(npe*(end[isection] - start[isection] + 1), -1);
        if (cgp_elements_read_data(fn, B, Z, S, start[isection], end[isection], &Elements[isection][0])) cgp_error_exit();
      }
    }
  }

  // Read the number of boundary conditions.
  if (cg_nbocos(fn, B, Z, &nbocos)) cg_error_exit();

  // Allocate boundary condition data.
  boconame.resize(nbocos);
  ptset_type.resize(nbocos);
  npnts.resize(nbocos);
  pnts.resize(nbocos);

  // Loop over boundary conditions
  for (int iboco = 0; iboco < nbocos; iboco++)
  {
    // Set boundary condition index number.
    BC = iboco + 1;

    // Read boundary condition info.
    if (cg_boco_info(fn, B, Z, BC, boconame_tmp, &bocotype, &ptset_type[iboco], &npnts[iboco], &NormalIndex, &NormalListSize, &NormalDataType, &ndataset)) cg_error_exit();
    boconame[iboco] = boconame_tmp;
    switch (ptset_type[iboco])
    {
      case PointRange: case ElementRange:
        break;
      case PointList: case ElementList:
        break;
      default: ;
    }

    // Allocate and read index of boundary elements.
    pnts[iboco].resize(npnts[iboco]);
    if (cg_boco_read(fn, B, Z, BC, &pnts[iboco][0], nullptr)) cg_error_exit();
  }

  // Initialize the number of cells/faces and c2n/f2n total index.
  num_cells = c2n_total_index = 0;
  num_faces = f2n_total_index = 0;

  // Loop over sections.
  for (int isection = 0; isection < nsections; isection++)
  {
    // Check start/end index.
    if (start[isection] > end[isection]) continue;

    // Check element range.
    if (elem_range_max[isection] <= size[1])
    {
      // Count the number of cells at this section.
      num_local = end[isection] - start[isection] + 1;

      // Update the number of cells and c2n total index.
      num_cells += num_local;
      c2n_total_index += Elements[isection].size();
      if (type[isection] == MIXED) c2n_total_index -= num_local;
    }
    else
    {
      // Count the number of faces at this section.
      num_local = end[isection] - start[isection] + 1;

      // Count the number of faces and f2n total index.
      num_faces += num_local;
      f2n_total_index += Elements[isection].size();
      if (type[isection] == MIXED) f2n_total_index -= num_local;
    }
  }
  
  // Read the number of field.
  int num_fields;
  S = 1;
  if (cg_nfields(fn, B, Z, S, &num_fields)) cgp_error_exit();
  solution_.resize(num_fields);
  for (int_t idata = 0; idata < num_fields; idata++)
  {
    solution_[idata].resize(num_cells, 0.0);
  }

  ////////////////////////////////////////////////////////////
  //                    GRID DATA SETTING                   //
  ////////////////////////////////////////////////////////////

  // Set grid dimension.
  dimension_ = cell_dim;

  // Set and allocate grid node data.
  num_total_nodes_ = int_t(size[0]);
  num_nodes_ = node_range_max - node_range_min + 1;
  node_centroid_.resize(dimension_);
  node_global_index_.resize(num_nodes_);
  for (int_t idim = 0; idim < dimension_; idim++)
  {
    node_centroid_[idim].resize(num_nodes_);
  }

  // Set and allocate grid cell data.
  num_total_cells_ = int_t(size[1]);
  num_cells_ = num_cells;
  cell_elem_type_.resize(num_cells_);
  cell_to_node_ptr_.resize(num_cells_+1);
  cell_to_node_ind_.resize(c2n_total_index);
  cell_global_index_.resize(num_cells_);
  std::vector<int_t> cell_dist(ndomain+1, 0);

  for (int_t idomain = 0; idomain < ndomain; idomain++)
  {
    cell_dist[idomain+1] = cell_dist[idomain] + num_cells;
    cell_dist[idomain+1] = std::min(cell_dist[idomain+1], num_total_cells_);
  }

  for (int_t icell = 0; icell < num_cells_; icell++)
  {
    cell_global_index_[icell] = cell_dist[my_rank] + icell;
  }

  // Set and allocate grid face data.
  num_faces_ = num_faces;
  face_to_node_ptr_.resize(num_faces_+1);
  face_to_node_ind_.resize(f2n_total_index);
  face_elem_type_.resize(num_faces_);

  // Set and allocate grid patch data.
  num_patches_ = int_t(nbocos);
  patch_name_.resize(num_patches_);
  patch_to_face_ptr_.resize(num_patches_+1);

  // Loop over nodes.
  for (int_t inode = 0; inode < num_nodes_; inode++)
  {
    // Set node centroid.
    for (int_t idim = 0; idim < dimension_; idim++)
    {
      node_centroid_[idim][inode] = coord_array[idim][inode];
    }
  }

  // Initialize cell index and c2n pointer.
  icell = 0;
  cell_to_node_ptr_[0] = 0;

  // Loop over sections.
  for (int isection = 0; isection < nsections; isection++)
  {
    // Check element range.
    if (elem_range_max[isection] <= size[1])
    {
      // Initialize temporary index.
      index_tmp = 0;

      // Loop over global elements.
      for (cgsize_t iglobal = start[isection]; iglobal <= end[isection]; iglobal++)
      {
        // Check section type.
        if (type[isection] == MIXED)
        {
          // Set cell element type.
          cell_elem_type_[icell] = Elements[isection][index_tmp];
          ++index_tmp; // Update temporary index.
        }
        else
        {
          // Set cell element type.
          cell_elem_type_[icell] = type[isection];
        }

        // Get index size.
        index_size = ElemType::getIndexSize(cell_elem_type_[icell]);

        // Set cell to node pointer.
        c2n_total_index = cell_to_node_ptr_[icell] + index_size;
        cell_to_node_ptr_[icell+1] = c2n_total_index;

        // Set cell to node index. (index style change F->C)
        for (int_t index = 0; index < index_size; index++)
        {
          c2n_index = cell_to_node_ptr_[icell] + index;
          cell_to_node_ind_[c2n_index] = Elements[isection][index_tmp] - 1;
          ++index_tmp; // Update temporary index.
        }

        // Update cell index.
        ++icell;
      }
    }
  }


  // Initialize face index and f2n pointer.
  iface = 0;
  face_to_node_ptr_[0] = 0;
  patch_to_face_ptr_[0] = 0;

  // Loop over boundary conditions.
  for (int_t iboco = 0; iboco < nbocos; iboco++)
  {
    // Set patch name.
    patch_name_[iboco] = boconame[iboco];

    // Loop over sections.
    for (int isection = 0; isection < nsections; isection++)
    {
      // Check element range.
      if (elem_range_min[isection] > size[1])
      {
        // Initialize temporary index.
        index_tmp = 0;

        // Loop over global elements.
        for (cgsize_t iglobal = start[isection]; iglobal <= end[isection]; iglobal++)
        {
          // Initialize boundary condition flag.
          boco_flag = false;

          // Check ptset_type.
          if (ptset_type[iboco] == PointRange || ptset_type[iboco] == ElementRange)
          {
            // Check pnts range.
            if (iglobal >= pnts[iboco][0] && iglobal <= pnts[iboco][1]) boco_flag = true;
          }
          else if (ptset_type[iboco] == PointList || ptset_type[iboco] == ElementList)
          {
            // Loop over pnts.
            for (cgsize_t ipnt = 0; ipnt < npnts[iboco]; ipnt++)
            {
              // Check pnts index.
              if (iglobal == pnts[iboco][ipnt]) boco_flag = true;
            }
          }

          // Check boundary flag.
          if (boco_flag)
          {
            // Check section type.
            if (type[isection] == MIXED)
            {
              // Set face element type.
              face_elem_type_[iface] = Elements[isection][index_tmp];
              ++index_tmp;
            }
            else
            {
              // Set face element type.
              face_elem_type_[iface] = type[isection];
            }

            // Get index size.
            index_size = ElemType::getIndexSize(face_elem_type_[iface]);

            // Set face to node pointer.
            f2n_total_index = face_to_node_ptr_[iface] + index_size;
            face_to_node_ptr_[iface+1] = f2n_total_index;

            // Set face to node index
            for (int_t index = 0; index < index_size; index++)
            {
              f2n_index = face_to_node_ptr_[iface] + index;
              face_to_node_ind_[f2n_index] = Elements[isection][index_tmp] - 1;
              ++index_tmp;
            }

            // Update face index.
            ++iface;
          }
          else
          {
            // Check section type.
            if (type[isection] == MIXED)
            {
              // Get index size.
              index_size = ElemType::getIndexSize(Elements[isection][index_tmp]);
              ++index_tmp;
            }
            else
            {
              index_size = ElemType::getIndexSize(type[isection]);
            }

            // Update temporary index.
            index_tmp += index_size;
          }
        }
      }
    }
    // Update patch to face pointer.
    patch_to_face_ptr_[iboco+1] = iface;
  }

  // Close CGNS file.
  if (cgp_close(fn)) cgp_error_exit();

  volume_node_local_start_ = 0;
  volume_node_local_end_ = num_nodes_;
  for (int_t idomain = 0; idomain < ndomain; idomain++)
  {
    int_t num_node = num_nodes_;
    MPI::COMM_WORLD.Bcast(&num_node, 1, MPIDataType<int_t>(), idomain);
    if (idomain < my_rank)
    {
      volume_node_local_start_ += num_node;
      volume_node_local_end_ += num_node;
    }
  }

  if (dimension_ == 2)
  {
    volume_cell_type_list_.reserve(2);
    volume_cell_type_list_.push_back(ElemType::TRIS);
    volume_cell_type_list_.push_back(ElemType::QUAD);
  }
  else if (dimension_ == 3)
  {
    volume_cell_type_list_.reserve(3);
    volume_cell_type_list_.push_back(ElemType::TETS);
    volume_cell_type_list_.push_back(ElemType::PYRA);
    volume_cell_type_list_.push_back(ElemType::PRIS);
    volume_cell_type_list_.push_back(ElemType::HEXA);
  }

  int_t num_cell_type = volume_cell_type_list_.size();
  volume_cell_list_.resize(num_cell_type);
  volume_cell_global_ptr_.resize(num_cell_type+1, 0);
  volume_cell_local_start_.resize(num_cell_type, 0);
  volume_cell_local_end_.resize(num_cell_type, 0);
  for (int_t itype = 0; itype < num_cell_type; itype++)
  {
    for (int_t icell = 0; icell < num_cells_; icell++)
    {
      if (volume_cell_type_list_[itype] == cell_elem_type_[icell])
      {
        volume_cell_list_[itype].push_back(icell);
      }
    }

    volume_cell_local_start_[itype] = volume_cell_global_ptr_[itype];
    volume_cell_local_end_[itype] = volume_cell_local_start_[itype] + volume_cell_list_[itype].size();

    for (int_t idomain = 0; idomain < ndomain; idomain++)
    {
      int_t num_cell = volume_cell_list_[itype].size();
      MPI::COMM_WORLD.Bcast(&num_cell, 1, MPIDataType<int_t>(), idomain);
      if (idomain < my_rank)
      {
        volume_cell_local_start_[itype] += num_cell;
        volume_cell_local_end_[itype] += num_cell;
      }
    }

    int_t num_cell_total;
    int_t num_cell = volume_cell_list_[itype].size();
    MPI::COMM_WORLD.Allreduce(&num_cell, &num_cell_total, 1, MPIDataType<int_t>(), MPI_SUM);
    volume_cell_global_ptr_[itype+1] = volume_cell_global_ptr_[itype] + num_cell_total;
  }

  volume_node_post_index_.resize(num_nodes_, -1);
  for (int_t inode = 0; inode < num_nodes_; inode++)
  {
    //volume_node_post_index_[inode] = inode + volume_node_local_start_;
    volume_node_post_index_[inode] = inode;
  }

  volume_face_global_ptr_.resize(num_patches_);
  volume_face_local_start_.resize(num_patches_);
  volume_face_local_end_.resize(num_patches_);
  volume_face_list_.resize(num_patches_);

  // Set volume face type list.
  volume_face_type_list_.resize(num_patches_);
  for (int_t ipatch = 0; ipatch < num_patches_; ipatch++)
  {
    if (dimension_ == 2)
    {
      volume_face_type_list_[ipatch].reserve(1);
      volume_face_type_list_[ipatch].push_back(ElemType::LINE);
    }
    else if (dimension_ == 3)
    {
      volume_face_type_list_[ipatch].reserve(2);
      volume_face_type_list_[ipatch].push_back(ElemType::TRIS);
      volume_face_type_list_[ipatch].push_back(ElemType::QUAD);
    }
  }

  for (int_t ipatch = 0; ipatch < num_patches_; ipatch++)
  {
    // Allocate and initialize volume face data.
    const int_t &num_face_type = volume_face_type_list_[ipatch].size();
    volume_face_global_ptr_[ipatch].resize(num_face_type+1, 0);
    volume_face_local_start_[ipatch].resize(num_face_type, 0);
    volume_face_local_end_[ipatch].resize(num_face_type, 0);
    volume_face_list_[ipatch].resize(num_face_type);

    // Set volume face global pointer.
    if (ipatch != 0)
    {
      volume_face_global_ptr_[ipatch].front() = volume_face_global_ptr_[ipatch-1].back();
    }

    // Loop over face type list.
    for (int_t itype = 0; itype < num_face_type; itype++)
    {
      // Set volume face list.
      for (int_t iface = patch_to_face_ptr_[ipatch];
                 iface < patch_to_face_ptr_[ipatch+1];
                 iface++)
      {
        if (volume_face_type_list_[ipatch][itype] == face_elem_type_[iface])
        {
          volume_face_list_[ipatch][itype].push_back(iface);
        }
      }

      // Initialize volume face local start/end index.
      volume_face_local_start_[ipatch][itype] = volume_face_global_ptr_[ipatch][itype];
      volume_face_local_end_[ipatch][itype] = volume_face_global_ptr_[ipatch][itype] + volume_face_list_[ipatch][itype].size();

      // Update volume face local start/end index.
      for (int_t idomain = 0; idomain < ndomain; idomain++)
      {
        int_t num_face = volume_face_list_[ipatch][itype].size();
        MPI::COMM_WORLD.Bcast(&num_face, 1, MPIDataType<int_t>(), idomain);
        if (idomain < my_rank)
        {
          volume_face_local_start_[ipatch][itype] += num_face;
          volume_face_local_end_[ipatch][itype] += num_face;
        }
      }
      
      // Update volume face global pointer.
      int_t num_face_total;
      int_t num_face = volume_face_list_[ipatch][itype].size();
      MPI::COMM_WORLD.Allreduce(&num_face, &num_face_total, 1, MPIDataType<int_t>(), MPI_SUM);
      volume_face_global_ptr_[ipatch][itype+1] = volume_face_global_ptr_[ipatch][itype] + num_face_total;
    }
  }
}

void Data::allocatePostData()
{
  // Allocate & initialize cell data & connectivity.
  int_t index_size_max0 = 0;
  int_t index_size_max1 = 0;

  for (int_t itype = 0; itype < volume_cell_type_list_.size(); itype++)
  {
    // Set element type.
    const int_t& elem_type = volume_cell_type_list_[itype];

    // Set and check cell range min/max.
    cgsize_t cell_range_min = volume_cell_global_ptr_[itype]+1;
    cgsize_t cell_range_max = volume_cell_global_ptr_[itype+1];
    if (cell_range_min > cell_range_max) continue;

    // Set cell start/end index.
    cgsize_t cell_start = volume_cell_local_start_[itype]+1;
    cgsize_t cell_end = volume_cell_local_end_[itype];
    int_t index_size = ElemType::getIndexSize(elem_type);

    // Set index size of sorted data/cell elements.
    index_size_max0 = std::max(index_size_max0, cell_end - cell_start + 1);
    index_size_max1 = std::max(index_size_max1, index_size*(cell_end - cell_start + 1));
  }
  sorted_data_.resize(index_size_max0, 0.0);
  cell_elements_.resize(index_size_max1, 0);

  // Allocate & initialize face connectivity.
  int_t index_size_max = 0;
  for (int_t ipatch = 0; ipatch < num_patches_; ipatch++)
  {
    // Loop over face type list.
    for (int_t itype = 0; itype < volume_face_type_list_[ipatch].size(); itype++)
    {
      // Set element type.
      const int_t& elem_type = volume_face_type_list_[ipatch][itype];

      // Set and check face range min/max.
      cgsize_t face_range_min = volume_face_global_ptr_[ipatch][itype];
      cgsize_t face_range_max = volume_face_global_ptr_[ipatch][itype];
      if (face_range_min > face_range_max) continue;

      // Set face start/end index.
      cgsize_t face_start = volume_face_local_start_[ipatch][itype];
      cgsize_t face_end = volume_face_local_end_[ipatch][itype];
      int_t index_size = ElemType::getIndexSize(elem_type);

      // Set index size max of face elements.
      index_size_max = std::max(index_size_max, index_size*(face_end - face_start + 1));
    }
  }
  face_elements_.resize(index_size_max, 0);

  // Allocate coordinate x/y/z.
  coordinate_x_.resize(num_nodes_, 0.0);
  coordinate_y_.resize(num_nodes_, 0.0);
  coordinate_z_.resize(num_nodes_, 0.0);
}

void Data::open(const std::string& filename)
{
  MASTER_MESSAGE("Averagine volume files .......\n");
  // Open CGNS file.
  int cell_dim = dimension_;
  int phys_dim = dimension_;
  if (cgp_open(filename.c_str(), CG_MODE_WRITE, &fn_) || cg_base_write(fn_, "Base", cell_dim, phys_dim, &B_)) cgp_error_exit();
}

void Data::writeZone()
{
  // Write zone.
  std::array<cgsize_t, 3> size;
  size[0] = num_total_nodes_;
  size[1] = num_total_cells_;
  size[2] = 0;
  if (cg_zone_write(fn_, B_, "Zone", &size[0], Unstructured, &Z_)) cgp_error_exit();
}

void Data::writeCoordinates()
{
  if (dimension_ == 2)
  {
    // Create coordinates data in parallel.
    if (cgp_coord_write(fn_, B_, Z_, RealDouble, "CoordinateX", &Cx_) ||
        cgp_coord_write(fn_, B_, Z_, RealDouble, "CoordinateY", &Cy_)) cgp_error_exit();

    // Set node start/end index.
    cgsize_t node_start = volume_node_local_start_ + 1;
    cgsize_t node_end = volume_node_local_end_;

    // Set node coordinate for CGNS
    for (int_t inode = 0; inode < num_nodes_; inode++)
    {
      coordinate_x_[inode] = node_centroid_[0][inode];
      coordinate_y_[inode] = node_centroid_[1][inode];
    }

    // Write coordinates data in parallel.
    if (node_start > node_end)
    {
      if (cgp_coord_write_data(fn_, B_, Z_, Cx_, nullptr, nullptr, nullptr) ||
          cgp_coord_write_data(fn_, B_, Z_, Cy_, nullptr, nullptr, nullptr)) cgp_error_exit();
    }
    else
    {
      if (cgp_coord_write_data(fn_, B_, Z_, Cx_, &node_start, &node_end, &coordinate_x_[0]) ||
          cgp_coord_write_data(fn_, B_, Z_, Cy_, &node_start, &node_end, &coordinate_y_[0])) cgp_error_exit();
    }
  }
  else if (dimension_ == 3)
  {
    // Create coordinates data in parallel.
    if (cgp_coord_write(fn_, B_, Z_, RealDouble, "CoordinateX", &Cx_) ||
        cgp_coord_write(fn_, B_, Z_, RealDouble, "CoordinateY", &Cy_) ||
        cgp_coord_write(fn_, B_, Z_, RealDouble, "CoordinateZ", &Cz_)) cgp_error_exit();

    // Set node start/end index.
    cgsize_t node_start = volume_node_local_start_ + 1;
    cgsize_t node_end = volume_node_local_end_;

    // Set node coordinates for CGNS.
    for (int_t inode = 0; inode < num_nodes_; inode++)
    {
      coordinate_x_[inode] = node_centroid_[0][inode];
      coordinate_y_[inode] = node_centroid_[1][inode];
      coordinate_z_[inode] = node_centroid_[2][inode];
    }

    // Write coordinates data in parallel.
    if (node_start > node_end)
    {
      if (cgp_coord_write_data(fn_, B_, Z_, Cx_, nullptr, nullptr, nullptr) ||
          cgp_coord_write_data(fn_, B_, Z_, Cy_, nullptr, nullptr, nullptr) ||
          cgp_coord_write_data(fn_, B_, Z_, Cz_, nullptr, nullptr, nullptr)) cgp_error_exit();
    }
    else
    {
      if (cgp_coord_write_data(fn_, B_, Z_, Cx_, &node_start, &node_end, &coordinate_x_[0]) ||
          cgp_coord_write_data(fn_, B_, Z_, Cy_, &node_start, &node_end, &coordinate_y_[0]) ||
          cgp_coord_write_data(fn_, B_, Z_, Cz_, &node_start, &node_end, &coordinate_z_[0])) cgp_error_exit();
    }
  }
}

void Data::writeCellElements()
{
  // Loop over cell type list.
  for (int_t itype = 0; itype < volume_cell_type_list_.size(); itype++)
  {
    // Set element type.
    const int_t& elem_type = volume_cell_type_list_[itype];

    // Set and check cell range min/max.
    cgsize_t cell_range_min = volume_cell_global_ptr_[itype]+1;
    cgsize_t cell_range_max = volume_cell_global_ptr_[itype+1];
    if (cell_range_min > cell_range_max) continue;

    // Create section in parallel.
    std::string name = "Volume_" + ElemType::getName(elem_type);
    ElementType_t type = static_cast<ElementType_t>(elem_type);
    if (cgp_section_write(fn_, B_, Z_, name.c_str(), type, cell_range_min, cell_range_max, 0, &S_)) cgp_error_exit();

    // Set cell start/end index.
    cgsize_t cell_start = volume_cell_local_start_[itype]+1;
    cgsize_t cell_end = volume_cell_local_end_[itype];

    // Set element connectivity for CGNS.
    int_t index = -1;
    for (auto&& icell : volume_cell_list_[itype])
    {
      for (int_t c2n_index = cell_to_node_ptr_[icell];
                 c2n_index < cell_to_node_ptr_[icell+1];
                 c2n_index++)
      {
        ++index;
        const int_t& inode = cell_to_node_ind_[c2n_index];
        cell_elements_[index] = inode + 1;
      }
    }

    // Write section data in parallel.
    if (cell_start > cell_end )
    {
      if (cgp_elements_write_data(fn_, B_, Z_, S_, cell_range_min, cell_range_max, nullptr)) cgp_error_exit();
    }
    else
    {
      if (cgp_elements_write_data(fn_, B_, Z_, S_, cell_start, cell_end, &cell_elements_[0])) cgp_error_exit();
    }
  }
}

void Data::writeFaceElements()
{
  // Loop over patches.
  for (int_t ipatch = 0; ipatch < num_patches_; ipatch++)
  {
    // Loop over face type list.
    for (int_t itype = 0; itype < volume_face_type_list_[ipatch].size(); itype++)
    {
      // Set element type.
      const int_t& elem_type = volume_face_type_list_[ipatch][itype];

      // Set and check face range min/max.
      cgsize_t face_range_min = volume_face_global_ptr_[ipatch][itype]+1;
      cgsize_t face_range_max = volume_face_global_ptr_[ipatch][itype+1];
      if (face_range_min > face_range_max) continue;
      face_range_min += num_total_cells_;
      face_range_max += num_total_cells_;

      // Create section in parallel.
      std::string name = patch_name_[ipatch] + "_" + ElemType::getName(elem_type);
      ElementType_t type = static_cast<ElementType_t>(elem_type);
      if (cgp_section_write(fn_, B_, Z_, name.c_str(), type, face_range_min, face_range_max, 0, &S_)) cgp_error_exit();

      // Set face start/end index.
      cgsize_t face_start = volume_face_local_start_[ipatch][itype]+1;
      cgsize_t face_end = volume_face_local_end_[ipatch][itype];
      face_start += num_total_cells_;
      face_end += num_total_cells_;

      // Set element connectivity for CGNS.
      int_t index = -1;
      for (auto&& iface : volume_face_list_[ipatch][itype])
      {
        for (int_t f2n_index = face_to_node_ptr_[iface];
                   f2n_index < face_to_node_ptr_[iface+1];
                   f2n_index++)
        {
          ++index;
          const int_t& inode = face_to_node_ind_[f2n_index];
          face_elements_[index] = inode + 1;
        }
      }

      // Write section data in parallel.
      if (face_start > face_end)
      {
        if (cgp_elements_write_data(fn_, B_, Z_, S_, face_range_min, face_range_max, nullptr)) cgp_error_exit();
      }
      else
      {
        if (cgp_elements_write_data(fn_, B_, Z_, S_, face_start, face_end, &face_elements_[0])) cgp_error_exit();
      }
    }
  }
}

void Data::writeZoneBC()
{
  // Loop over patches.
  for (int_t ipatch = 0; ipatch < num_patches_; ipatch++)
  {
    // Set and check boundary condition start/end.
    cgsize_t boco_start = volume_face_global_ptr_[ipatch][0]+1;
    cgsize_t boco_end = volume_face_global_ptr_[ipatch].back();
    boco_start += num_total_cells_;
    boco_end += num_total_cells_;
    if (boco_start > boco_end) continue;

    // Create boundary condition in parallel.
    const std::string name = patch_name_[ipatch];
    std::array<cgsize_t, 2> pnts = {boco_start, boco_end};
    if (cg_boco_write(fn_, B_, Z_, name.c_str(), BCTypeUserDefined, PointRange, 2, &pnts[0], &BC_)) cgp_error_exit();

    // Check dimension.
    if (dimension_ == 2)
    {
      if (cg_boco_gridlocation_write(fn_, B_, Z_, BC_, EdgeCenter)) cgp_error_exit();
    }
    else if (dimension_ == 3)
    {
      if (cg_boco_gridlocation_write(fn_, B_, Z_, BC_, FaceCenter)) cgp_error_exit();
    }
  }
}

void Data::writeSolutionHeader()
{
  if (cg_sol_write(fn_, B_, Z_, "Solution", CellCenter, &S_)) cgp_error_exit();

  return;

  // Write cell color.
  
  // Create field data in parallel.
  if (cgp_field_write(fn_, B_, Z_, S_, RealDouble, "Cellcolor", &F_)) cgp_error_exit();

  // Loop over cell type list.
  for (int_t itype = 0; itype < volume_cell_type_list_.size(); itype++)
  {
    // Set and check cell range min/max.
    cgsize_t cell_range_min = volume_cell_global_ptr_[itype]+1;
    cgsize_t cell_range_max = volume_cell_global_ptr_[itype+1];
    if (cell_range_min > cell_range_max) continue;

    // Set cell start/end index.
    cgsize_t cell_start = volume_cell_local_start_[itype]+1;
    cgsize_t cell_end = volume_cell_local_end_[itype];

    // Set cell data for CGNS.
    int_t index = -1;
    for (auto &&icell : volume_cell_list_[itype])
    {
      ++index;
      sorted_data_[index] = double(cell_color_[icell]);
    }

    // Write field data in paralle.
    if (cell_start > cell_end)
    {
      if (cgp_field_write_data(fn_, B_, Z_, S_, F_, nullptr, nullptr, nullptr)) cgp_error_exit();
    }
    else
    {
      if (cgp_field_write_data(fn_, B_, Z_, S_, F_, &cell_start, &cell_end, &sorted_data_[0])) cgp_error_exit();
    }
  }
}

void Data::writeIterData()
{
  if (cg_simulation_type_write(fn_, B_, NonTimeAccurate)) cgp_error_exit();

  // Access BaseIterativeData node.
  cgsize_t dimension_vector = 1;
  if (cg_biter_write(fn_, B_, "TimeIterValue", 1)) cgp_error_exit();
  if (cg_goto(fn_, B_, "BaseIterativeData_t", 1, "end")) cgp_error_exit();

  // Write time value.
  real_t time = 0.0;
  if (cg_array_write("TimeValues", RealDouble, 1, &dimension_vector, &time)) cgp_error_exit();

  // Write physical iteration value.
  int_t physical_iter = 0;
  if (cg_array_write("IterartiveValues", Integer, 1, &dimension_vector, &physical_iter)) cgp_error_exit();

  // Write pseudo iteration value.
  int_t pseudo_iter = 0;
  if (cg_array_write("PseudoIterationValue", Integer, 1, &dimension_vector, &pseudo_iter)) cgp_error_exit();
}

void Data::readSolutionData(const std::string &filename)
{
  std::string CGNSfilename = filename + ".cgns";
  CGNSfilename = "./volume/"+CGNSfilename;

  // Set rank of the processor and size of all processors.
  int_t my_rank = MPI::COMM_WORLD.Get_rank();
  int_t ndomain = MPI::COMM_WORLD.Get_size();

  // Set CGNS MPI communicator.
  cgp_mpi_comm(MPI::COMM_WORLD);

  // Set variables for CGNS.
  int fn = 1;
  int B = 1;
  int Z = 1;
  int S = 1;
  int F;

  // Open CGNS file.
  fn = 1;
  if (cgp_open(CGNSfilename.c_str(), CG_MODE_READ, &fn)) cgp_error_exit();

  // Read zone.
  char zone_name[33];
  std::array<cgsize_t, 3> size;
  if (cg_zone_read(fn, B, Z, zone_name, &size[0])) cgp_error_exit();
  int_t num_total_cells = int_t(size[1]);

  // Set the number of cells with cell dist data.
  int_t num_cells;
  int_t num_cells_max = 0;
  num_cells = int_t(ceil(real_t(num_total_cells)/real_t(ndomain)));
  std::vector<int_t> cell_dist(ndomain+1, 0);
  for (int_t idomain = 0; idomain < ndomain; idomain++)
  {
    cell_dist[idomain+1] = cell_dist[idomain] + num_cells;
    cell_dist[idomain+1] = std::min(cell_dist[idomain+1], num_total_cells);
    num_cells_max = std::max(num_cells_max, cell_dist[idomain+1] - cell_dist[idomain]);
  }
  num_cells = cell_dist[my_rank+1] - cell_dist[my_rank];

  // Read the number of field.
  int num_fields;
  if (cg_nfields(fn, B, Z, S, &num_fields)) cgp_error_exit();
  num_solutions_ = num_fields;

  // Allocate variables for communication.
  std::vector<real_t> cell_data(num_cells, 0.0);

  // Declare solution flag.
  bool *solution_flag = new bool [num_solutions_];
  std::fill_n(solution_flag, num_solutions_, false);

  solution_names_.resize(num_solutions_);
  std::vector<std::vector<real_t>> solution_tmp;
  solution_tmp.resize(num_solutions_);
  for (int_t isoln = 0; isoln < num_solutions_; isoln++)
  {
    solution_tmp[isoln].resize(num_cells_, 0.0);
  }

  // Loop over field
  for (int_t ifield = 0; ifield < num_fields; ifield++)
  {
    // Update F value.
    F = ifield + 1;

    // Read field name & data type.
    DataType_t data_type;
    char field_name[33];
    if (cg_field_info(fn, B, Z, S, F, &data_type, field_name)) cgp_error_exit();
    solution_names_[ifield] = field_name;

    // Set and check cell range min/max.
    cgsize_t cell_range_min = cell_dist[my_rank]+1;
    cgsize_t cell_range_max = cell_dist[my_rank+1];

    // Read field data.
    if (cgp_field_read_data(fn, B, Z, S, F, &cell_range_min, &cell_range_max, &cell_data[0])) cgp_error_exit();

    // Loop over domains
    for (int_t icell = 0; icell < num_cells_; icell++)
    {
      int_t index = cell_global_index_[icell] - cell_dist[my_rank];
      solution_tmp[ifield][icell] = cell_data[icell];
    }
    for (int_t icell = 0; icell < num_cells_; icell++)
    {
      solution_[ifield][icell] += solution_tmp[ifield][icell];
    }
  }
}

void Data::writeEqtnData(const std::vector<std::string>& name, const std::vector<std::vector<real_t>>& data)
{
  // Loop over data column.
  for (int_t icol = 0; icol < data.size(); icol++)
  {
    // Create field data in parallel.
    if (cgp_field_write(fn_, B_, Z_, S_, RealDouble, name[icol].c_str(), &F_)) cgp_error_exit();

    // Loop over cell type list.
    for (int_t itype = 0; itype < volume_cell_type_list_.size(); itype++)
    {
      cgsize_t cell_range_min = volume_cell_global_ptr_[itype]+1;
      cgsize_t cell_range_max = volume_cell_global_ptr_[itype+1];
      if (cell_range_min > cell_range_max) continue;

      // Set cell start/end index.
      cgsize_t cell_start = volume_cell_local_start_[itype]+1;
      cgsize_t cell_end = volume_cell_local_end_[itype];

      // Set cell data for CGNS.
      int_t index = -1;
      for (auto &&icell : volume_cell_list_[itype])
      {
        ++index;
        sorted_data_[index] = data[icol][icell];
      }

      // Write field data in parallel.
      if (cell_start > cell_end)
      {
        if (cgp_field_write_data(fn_, B_, Z_, S_, F_, nullptr, nullptr, nullptr)) cgp_error_exit();
      }
      else
      {
        if (cgp_field_write_data(fn_, B_, Z_, S_, F_, &cell_start, &cell_end, &sorted_data_[0])) cgp_error_exit();
      }
    }
  }
}

void Data::close()
{
  // Close CGNS file.
  if (cgp_close(fn_)) cgp_error_exit();
}

std::vector<int_t> Data::getDistribution(const int_t& num_data, const int_t& num_dist)
{
  // Compute interval.
  int_t interval = int_t(ceil(real_t(num_data)/real_t(num_dist)));

  // Declare and set dist data.
  std::vector<int_t> dist(num_dist+1, 0);
  for (int_t idist = 0; idist < num_dist; idist++)
  {
    dist[idist+1] = dist[idist] + interval;
    dist[idist+1] = std::min(dist[idist+1], num_data);
  }

  // Return dist data.
  return dist;
}
