#include "DataType.hpp"
#include "Data.hpp"
#include "Surface.hpp"

const std::string filename_ = "Post.cfg";

int main()
{
  // Initialize MPI
  MPI::Init();
  std::string name;
  Data data;
  Surface surface;

  surface.readInputFile(name);
  surface.readGridFile(name);
  surface.allocatePostData();
  name = "surface_average.cgns";
  surface.open(name);
  surface.writeZone();
  surface.writeCoordinates();
  surface.writeFaceElements();
  surface.writeSolutionHeader();
  surface.writeIterData();
  for (int_t ifile = surface.start_file_number_; ifile <= surface.end_file_number_; ifile++)
  {
    std::stringstream extension;
    extension << "_" << std::setw(8) << std::setfill('0') << ifile;
    name = "surface" + extension.str();
    surface.readSolutionData(name);
  }
  for (int_t idata = 0; idata < surface.num_solutions_; idata++)
  {
    for (int_t iface = 0; iface < surface.num_faces_; iface++)
      surface.solution_[idata][iface] /= real_t(surface.end_file_number_ - surface.start_file_number_ + 1);
  }
  surface.writeEqtnData(surface.solution_names_, surface.solution_);
  surface.close();

  data.readInputFile(name);
  data.readGridFile(name);
  data.allocatePostData();
  name = "volume_average.cgns";
  data.open(name);
  data.writeZone();
  data.writeCoordinates();
  data.writeCellElements();
  data.writeFaceElements();
  data.writeZoneBC();
  data.writeSolutionHeader();
  data.writeIterData();
  for (int_t ifile = data.start_file_number_; ifile <= data.end_file_number_; ifile++)
  {
    std::stringstream extension;
    extension << "_" << std::setw(8) << std::setfill('0') << ifile;
    name = "volume" + extension.str();
    data.readSolutionData(name);
  }
  for (int_t idata = 0; idata < data.num_solutions_; idata++)
  {
    for (int_t icell = 0; icell < data.num_cells_; icell++)
      data.solution_[idata][icell] /= real_t(data.end_file_number_ - data.start_file_number_ + 1);
  }
  data.writeEqtnData(data.solution_names_, data.solution_);
  data.close();

  // Finalize MPI
  MPI::Finalize();
  return 0;
}
