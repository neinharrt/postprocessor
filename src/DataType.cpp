#include "DataType.hpp"
#include "ElemType.hpp"

const std::string filename_ = "Post.cfg";

void printMessage(bool master_only, const std::string &str)
{
  // Get MPI rank.
  const int_t my_rank = MPI::COMM_WORLD.Get_rank();

  // Print message at master node.
  if (master_only)
  {
    if (my_rank == MASTER_NODE)
    {
      std::cout << str << std::flush;
    }
  }
  // Print message at local node.
  else
  {
    std::cout << str << std::flush;
  }
}

void ltrim(std::string &str)
{
  str.erase(str.begin(), std::find_if(str.begin(), str.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
}

void rtrim(std::string &str)
{
  str.erase(std::find_if(str.rbegin(), str.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), str.end());
}

void trim(std::string &str)
{
  ltrim(str);
  rtrim(str);
}

std::string trimmed(std::string str)
{
  trim(str);
  return str;
}

void printError(bool master_only, const std::string &str, const std::string &file, const int_t &line)
{
  // Print error message with file and line info.
  printMessage(master_only, '\n' + str + '\n');
  printMessage(master_only, "Program Aborting... ["+file+","+std::to_string(line)+"]\n\n");
  if (master_only) MPI::COMM_WORLD.Barrier();
  MPI::COMM_WORLD.Abort(EXIT_FAILURE);
}

std::string getValueFromKey(std::ifstream &infile, const std::string &key)
{
  std::string text;
  getline(infile, text);
  if (text.find(key+"=", 0) == std::string::npos)
    MASTER_ERROR("Key ("+key+") must be set ("+filename_+")");

  text = text.substr(key.length()+1);
  return trimmed(text);
}
