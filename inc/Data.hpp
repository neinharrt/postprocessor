#pragma once

#include "DataType.hpp"

class Data
{
  public:

    Data();

    ~Data();

  public:

    int_t dimension_;

    int_t num_patches_;

    std::vector<std::vector<real_t>> node_centroid_;

    int_t num_nodes_;

    int_t num_total_nodes_;

    int_t volume_node_local_start_;

    int_t volume_node_local_end_;

    std::vector<int_t> volume_node_post_index_;

    std::vector<int_t> node_global_index_;

    int_t num_total_cells_;

    int_t num_cells_;

    std::vector<int_t> volume_cell_type_list_;

    std::vector<int_t> volume_cell_global_ptr_;

    std::vector<int_t> volume_cell_local_start_;

    std::vector<int_t> volume_cell_local_end_;

    std::vector<std::vector<int_t>> volume_cell_list_;

    std::vector<int_t> cell_color_;

    std::vector<int_t> cell_elem_type_;

    std::vector<int_t> cell_global_index_;

    int_t num_faces_;

    std::vector<std::vector<int_t>> volume_face_type_list_;

    std::vector<std::vector<int_t>> volume_face_global_ptr_;

    std::vector<std::vector<int_t>> volume_face_local_start_;

    std::vector<std::vector<int_t>> volume_face_local_end_;

    std::vector<std::vector<std::vector<int_t>>> volume_face_list_;

    std::vector<int_t> face_elem_type_;

    std::vector<std::string> patch_name_;

    std::vector<int_t> cell_to_node_ptr_;

    std::vector<int_t> cell_to_node_ind_;

    std::vector<int_t> face_to_node_ptr_;

    std::vector<int_t> face_to_node_ind_;

    std::vector<int_t> patch_to_face_ptr_;

    std::vector<std::vector<real_t>> solution_;

    int_t num_solutions_;

    std::vector<std::string> solution_names_;

  public:

    int_t start_file_number_;

    int_t end_file_number_;

  public:

    void allocatePostData();

    void open(const std::string& filename);

    void writeZone();

    void writeCoordinates();

    void writeCellElements();

    void writeFaceElements();

    void writeZoneBC();

    void writeSolutionHeader();

    void writeIterData();

    void writeEqtnData(const std::vector<std::string>& name, const std::vector<std::vector<real_t>>& data);

    void close();

    void readGridFile(const std::string& filename);

    void readSolutionData(const std::string& filename);

    void readInputFile(std::string& name);

    std::vector<int_t> getDistribution(const int_t& num_data, const int_t& num_divide);

  protected:

    // CGNS file index
    int fn_;

    // CGNS base index
    int B_;

    // CGNS zone index
    int Z_;

    // CGNS coordinate X index
    int Cx_;

    // CGNS coordinate Y index
    int Cy_;

    // CGNS coordinate Z index
    int Cz_;

    // CGNS section/solution index
    int S_;

    // CGNS field index
    int F_;

    // CGNS boundary condition index
    int BC_;

    // Sorted data
    std::vector<double> sorted_data_;

    std::vector<cgsize_t> cell_elements_;

    std::vector<cgsize_t> face_elements_;

    std::vector<double> coordinate_x_;

    std::vector<double> coordinate_y_;

    std::vector<double> coordinate_z_;
};
