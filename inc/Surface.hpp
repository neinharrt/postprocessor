#pragma once

#include "DataType.hpp"

class Surface
{
  public:

    Surface();

    ~Surface();

  public:

    void allocatePostData();

    void open(const std::string &filename);

    void writeBasicData();

    void writeIterData();

    void writeEqtnFaceData(const std::string &name, const std::vector<real_t> &data);

    void writeEqtnCellData(const std::string &name, const std::vector<std::vector<real_t>> &data);

    void close();

    void writeZone();

    void writeCoordinates();

    void writeFaceElements();

    void writeSolutionHeader();

    void readGridFile(const std::string &filename);

    void readSolutionData(const std::string &filename);

    void readInputFile(std::string &name);

    void writeEqtnData(const std::vector<std::string> &name, const std::vector<std::vector<real_t>> &data);

    std::vector<int_t> getDistribution(const int_t& num_data, const int_t &num_divide);

  public:
    
    int fn_;

    int B_;

    int Z_;

    int Cx_;

    int Cy_;

    int Cz_;

    int S_;

    int F_;

    int BC_;

    std::vector<double> sorted_data_;

    std::vector<cgsize_t> face_elements_;

    std::vector<double> coordinate_x_;

    std::vector<double> coordinate_y_;

    std::vector<double> coordinate_z_;

  public:

    int_t start_file_number_;

    int_t end_file_number_;

    int_t dimension_;

    int_t cell_dim_;

    int_t phys_dim_;

    int_t num_patches_;

    std::vector<std::vector<real_t>> node_centroid_;

    std::vector<int_t> surface_face_global_ptr_;

    int_t num_global_bndry_nodes_;

    int_t surface_node_local_start_;

    int_t surface_node_local_end_;

    int_t num_bndry_nodes_;

    std::vector<int_t> surface_face_type_list_;

    std::vector<std::string> patch_name_;

    std::vector<int_t> surface_face_local_start_;

    std::vector<int_t> surface_face_local_end_;

    std::vector<std::vector<int_t>> surface_face_list_;

    std::vector<int_t> face_to_node_ptr_;

    std::vector<int_t> face_to_node_ind_;

    std::vector<std::vector<real_t>> solution_;

    int_t num_total_nodes_;

    int_t num_total_faces_;

    int_t num_nodes_;

    std::vector<int_t> face_elem_type_;

    int_t num_faces_;

    std::vector<std::string> solution_names_;

    int_t num_solutions_;

    std::vector<int_t> face_global_index_;
};
