#pragma once

#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

#include <cmath>
#include <limits>
#include <algorithm>
#include <iomanip>

#include <vector>
#include <mpi.h>
#include <pcgnslib.h>
#include <chrono>
#include <array>

typedef long int_t;
typedef double real_t;

typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::duration<real_t> TimeDuration;

template<class T> MPI::Datatype MPIDataType();
template<> inline MPI::Datatype MPIDataType<bool>()                      { return MPI::BOOL; };
template<> inline MPI::Datatype MPIDataType<char>()                      { return MPI::CHAR; };
template<> inline MPI::Datatype MPIDataType<wchar_t>()                   { return MPI::WCHAR; };
template<> inline MPI::Datatype MPIDataType<signed char>()               { return MPI::SIGNED_CHAR; };
template<> inline MPI::Datatype MPIDataType<signed short>()              { return MPI::SHORT; };
template<> inline MPI::Datatype MPIDataType<signed int>()                { return MPI::INT; };
template<> inline MPI::Datatype MPIDataType<signed long>()               { return MPI::LONG; };
template<> inline MPI::Datatype MPIDataType<unsigned char>()             { return MPI::UNSIGNED_CHAR; };
template<> inline MPI::Datatype MPIDataType<unsigned short>()            { return MPI::UNSIGNED_SHORT; };
template<> inline MPI::Datatype MPIDataType<unsigned int>()              { return MPI::UNSIGNED; };
template<> inline MPI::Datatype MPIDataType<unsigned long>()             { return MPI::UNSIGNED_LONG; };
template<> inline MPI::Datatype MPIDataType<float>()                     { return MPI::FLOAT; };
template<> inline MPI::Datatype MPIDataType<double>()                    { return MPI::DOUBLE; };
template<> inline MPI::Datatype MPIDataType<long double>()               { return MPI::LONG_DOUBLE; };

#define MASTER_NODE 0
#define MASTER_MESSAGE(str) printMessage(true, str)
#define MEMBER_MESSAGE(str) printMessage(false, str)
#define MASTER_ERROR(str) printError(true, str, __FILE__, __LINE__)
#define MEMBER_ERROR(str) printError(false, str, __FILE__, __LINE__)

void printMessage(bool master_only, const std::string &str);

void printError(bool master_only, const std::string &str, const std::string &file, const int_t &line);

std::string getValueFromKey(std::ifstream &infile, const std::string &key);

void ltrim(std::string &str);
