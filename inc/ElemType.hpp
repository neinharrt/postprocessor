#pragma once

#include "DataType.hpp"

class ElemType
{
  public:

    enum BaseType
    {
      LINE = BAR_2,
      TRIS = TRI_3,
      QUAD = QUAD_4,
      TETS = TETRA_4,
      PYRA = PYRA_5,
      PRIS = PENTA_6,
      HEXA = HEXA_8
    };

  public:

    static std::string getName(int_t elem_type);
    static int_t getIndexSize(int_t elem_type);
    static int_t getNumSubElems(int_t elem_type);
    static int_t getSubElemType(int_t elem_type, int_t isub);
    static const std::vector<int_t>& getSubElemIndex(int_t elem_type, int_t isub);
};
